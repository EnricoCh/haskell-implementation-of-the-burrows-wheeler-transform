-- Enrico Chiaramello

import Data.List
import Data.Maybe

rot :: [a] -> [a]
rot x = tail x ++ head x : []

base :: Ord a => [a] -> [[a]]
base s = sort $ take (length s) $ iterate rot s

markToken :: Integral a => a -> a -> [Char]
markToken compressionValue n =
        if (n `mod` compressionValue == 0) then "@" else " "

showcheckpoints :: Integer -> [Char] -> Integer -> String -> String
showcheckpoints compressionValue alfabeto index acc =
        let g = zip alfabeto (repeat acc) in
        (concat $ map (
         \(x,y) -> " " ++
                   (show $ length $ filter (== x) y)
        ) g)
        ++ " " ++
        if (index `mod` compressionValue == 0) then "@" else " "

--mk_bwt :: Int -> Integer -> [Char] -> [[Char]]
mk_bwt suffixcompressionValue checkpoitcompressionvalue s =
      let addtoken = markToken suffixcompressionValue in
      let checkpoitprinter =
                showcheckpoints checkpoitcompressionvalue $ sort $ nub $ s in
      map fst $ foldl ( \old (new,index) ->
          -- old_checkpoint accumula i count per costruire i checkpoint
          let old_checkpoint = if (length old > 0)
              then let ((_,_,_,_,_,_,_,old_checkpoint),_) = last old in old_checkpoint
              else "" in
          let checkp = old_checkpoint ++ [last new] in
          let suffixelement = fromJust (elemIndex '$' (reverse new)) in
          let res_index = show index
              bwt_complete_string = new
              first_column = head new
              last_column = last new
              suffix_element_value = show suffixelement
              suffex_keeper = addtoken suffixelement
              checkpoint_element = checkpoitprinter index old_checkpoint
              in
          let res = (res_index, bwt_complete_string,first_column,last_column,suffix_element_value,suffex_keeper,checkpoint_element,checkp)
          in
          old ++ [(res, "variabile inutile")]
      ) ([]) (zip (base s) [0..])


bwtprint list = foldl (\x y -> do x >> putStrLn y)  (return ()) $ map show list

bwt csuffix ccheckpoint s = bwtprint $ mk_bwt csuffix ccheckpoint s

a = mk_bwt 2 4 "biolo$"
b = bwtprint $ mk_bwt 1 4 "biolo$"
c = bwt 2 5 "biologia$"

heavytest x = bwt 10 50 $ (concat $ take x $ repeat "a") ++ "$"

heavytestnooutput x = seq (heavytest x) ("fine")

