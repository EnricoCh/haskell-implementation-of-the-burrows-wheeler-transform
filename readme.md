# **haskell implementation of the Burrows–Wheeler transform**
https://en.wikipedia.org/wiki/Burrows%E2%80%93Wheeler_transform
***

## USAGE
```
bwt x y "string$"
```
 - `x` is the compressison factor of the suffix array
 - `y` is the compression factor of the checkpoint array
 - the `$` character is needed at the end of the string
---
## TESTING
to load-test the implementation

```
heavytestnooutput x
```
 - `x` is the lenght of the string to test (it automatically generates a string of lenght `x` and applies 
 `\x -> bwt 1 1 (x ++ "$")`
  to it)
